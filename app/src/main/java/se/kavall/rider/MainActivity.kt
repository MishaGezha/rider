package se.kavall.rider

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_player.*
import timber.log.Timber


class MainActivity : AppCompatActivity()  {
    var isBound: Boolean = false
    lateinit var mService: RiderService
    lateinit var actionButton : FloatingActionButton
    var pref :SharedPreferences ?= null
    lateinit var myConnection: ServiceConnection
    var init = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(checkSelfPermission(ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(MANAGE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            pref = this.getSharedPreferences("tracks", Context.MODE_PRIVATE)
            pref?.edit()?.clear()?.apply()
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                    requestPermissions(
                        arrayOf(
                            ACCESS_FINE_LOCATION,
                            ACCESS_COARSE_LOCATION,
                            READ_EXTERNAL_STORAGE
                        ), 5
                    )
                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                    requestPermissions(
                        arrayOf(
                            ACCESS_BACKGROUND_LOCATION,
                            READ_EXTERNAL_STORAGE,
                            ACCESS_FINE_LOCATION
                        ), 4
                    )
                }
                else -> requestPermissions(arrayOf(READ_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION), 4)
            }
        }
        else onPostCreate()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            4 -> {
                if (checkAllPermission(grantResults)) {
                    onPostCreate()
                } else finishAffinity()
            }
            5 -> {
                if (checkAllPermission(grantResults)) {
                    if (checkSelfPermission(ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(arrayOf(ACCESS_BACKGROUND_LOCATION), 6)
                    } else onPostCreate()
                } else finishAffinity()
            }
            6 -> {
                if (checkAllPermission(grantResults)) {
                    onPostCreate()
                } else finishAffinity()
            }
        }
    }

    fun checkAllPermission(grantResults: IntArray): Boolean {
        for (result in grantResults) {
            if (result == PackageManager.PERMISSION_GRANTED) continue
            else return false
        }
        return true
    }

    @SuppressLint("HardwareIds")
    fun onPostCreate() {
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        createConnection()
        pref = getSharedPreferences("tracks", Context.MODE_PRIVATE)
        actionButton = findViewById(R.id.fab)

        actionButton.setOnClickListener {
            if (isBound) {
                if (mService.playing) actionButton.setImageResource(R.drawable.ic_play_arrow)
                else actionButton.setImageResource(R.drawable.ic_pause)
                mService.changePlayState()
            }
        }
        init = true
        val deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        val deviceDetails = DeviceDetails(deviceId)
        val remoteTree = TimberRemoteTree(deviceDetails)

        Timber.plant(remoteTree)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        createConnection()
        val intent = Intent(this, RiderService::class.java).also { intent ->
            bindService(intent, myConnection, 0)}
        ContextCompat.startForegroundService(applicationContext, intent)

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, IntentFilter("service-message"))
    }

    private val messageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            when(intent.getIntExtra("value", -1)) {
                0 -> actionButton.setImageResource(R.drawable.ic_play_arrow)
                1 -> actionButton.setImageResource(R.drawable.ic_pause)
                2 -> {
                    val songNumb = intent.getIntExtra("songNumb", -1)
                    val fragment =
                        supportFragmentManager.findFragmentById(R.id.nav_host_fragment)?.childFragmentManager?.fragments?.get(
                            0
                        )
                    if (fragment is PlayerFragment) {
                        fragment.onSongChange(songNumb)
                    }
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver)
        if (init) {
            mService.activityDestroyed = true
            mService.serviceDestroy()
            mService.isDestroyed = true
        }
        isBound = false
    }

    private fun createConnection() {
        myConnection = object : ServiceConnection {
            override fun onServiceConnected(className: ComponentName, service: IBinder) {
                val binder = service as RiderService.LocalBinder
                mService = binder.getService()
                mService.activityDestroyed = false

                if (mService.hasPlayed) actionButton.setImageResource(R.drawable.ic_pause)
                else actionButton.setImageResource(R.drawable.ic_play_arrow)
                tvSpeed.text = "speed: ${String.format("%.2f", mService.currentSpeed)} km/h"
                isBound = true
            }

            override fun onServiceDisconnected(className: ComponentName) {
                isBound = false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                findNavController(this, R.id.nav_host_fragment).navigate(R.id.settingsFragment)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}