package se.kavall.rider

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_player.*

class PlayerFragment : Fragment(), IBaseGpsListener {
    private var pref : SharedPreferences?= null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
        }
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = this.activity?.getSharedPreferences("tracks", Context.MODE_PRIVATE)
    }

    override fun onResume() {
        super.onResume()
        val numbPlayingSong = pref?.getInt("songNumb", 1)
        val songStrUri = pref?.getString("song$numbPlayingSong", null)
        if (songStrUri != null) {
            val songUri = Uri.parse(songStrUri)
            val uriDataAdapter = UriDataAdapter()
            val title = uriDataAdapter.getFileTitle(songUri, activity?.applicationContext!!)
            if (title[0] == 'a' && title[1] == 'u') {
                trackTitle.text =   uriDataAdapter.getTitleAuth(songUri, activity?.applicationContext!!)
            }
            else trackTitle.text = title
            val imageUri = uriDataAdapter.getAlbumImage(songUri, requireContext())
            if (imageUri != null) {
                albumImage?.setImageResource(R.drawable.plate)
            }
            else albumImage?.setImageURI(imageUri)

        }
    }

    fun onSongChange(songNumb: Int) {
        val songStrUri = pref?.getString("song$songNumb", null)
        if (songStrUri != null) {
            val songUri = Uri.parse(songStrUri)
            val uriDataAdapter = UriDataAdapter()
            trackTitle.text = uriDataAdapter.getTitleAuth(songUri, requireContext())
            albumImage?.setImageURI(uriDataAdapter.getAlbumImage(songUri, requireContext()))
            if (albumImage?.getDrawable() == null) {
                albumImage?.setImageResource(R.drawable.plate)
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        val myLocation = CLocation(location, true)
        val nCurrentSpeed = myLocation.speed.toDouble()
        val formattedSpeed = String.format("%.2f", nCurrentSpeed)
        activity?.findViewById<TextView>(R.id.tvSpeed)?.text = "speed: $formattedSpeed km/h"
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        return
    }

    override fun onProviderEnabled(provider: String) {
        return
    }

    override fun onProviderDisabled(provider: String) {
        return
    }

    override fun onGpsStatusChanged(p0: Int) {
        return
    }
}