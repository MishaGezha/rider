package se.kavall.rider

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.*
import android.location.Location
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.support.v4.media.session.MediaSessionCompat
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import java.util.*


class RiderService : Service(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener {
    var playing : Boolean = false
    var receiverExist : Boolean = false
    lateinit var mediaPlayer : MediaPlayer
    private val binder = LocalBinder()
    private var pref : SharedPreferences?= null
    var checkAmountRaise = arrayListOf(0, 0, 0)
    var checkAmountFall = arrayListOf(0, 0, 0)
    var speedUp = arrayOfNulls<Double>(3)
    private val speedDown = arrayOfNulls<Double>(3)
    var numbPlayingSong = 0
    var activityDestroyed = false
    private val CHANGE_SPEED = 4
    var hasPlayed = false
    var isDestroyed = false
    lateinit var receiver: BroadcastReceiver
    lateinit var timer: Timer
    var currentSpeed: Double = 0.0
    private var googleApiClient: GoogleApiClient? = null
    private var locationRequest: LocationRequest? = null


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        notification(!hasPlayed)
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder {
        activityDestroyed = false
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        pref = applicationContext.getSharedPreferences("tracks", MODE_PRIVATE)
        speedUp[0] = pref?.getString("speed_up1", "4")?.toDouble()!!
        speedUp[1] = pref?.getString("speed_up2", "7")?.toDouble()!!
        speedUp[2] = pref?.getString("speed_up3", "10")?.toDouble()!!

        speedDown[0] = pref?.getString("speed_down1", "2")?.toDouble()!!
        speedDown[1] = pref?.getString("speed_down2", "5")?.toDouble()!!
        speedDown[2] = pref?.getString("speed_down3", "7")?.toDouble()!!
        val songUri = pref?.getString("song1", null)
        setMusicPlayer()
        if (songUri != null) {
            setNewTrack(1)
        }

        notification(true)
        googleApiClient =
            GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        googleApiClient?.connect()
    }

    override fun onDestroy() {
        pref?.edit()?.putInt("songNumb", 1)?.apply()
        applicationContext.unregisterReceiver(receiver)
        super.onDestroy()
    }

    private fun setMusicPlayer() {
        mediaPlayer = MediaPlayer().apply {
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build()
            )
        }
    }

    fun setNewTrack(songNumb: Int) {
        numbPlayingSong = songNumb
        pref?.edit()?.putInt("songNumb", songNumb)?.apply()
        val songUri = pref?.getString("song$songNumb", null)
        numbPlayingSong = songNumb
        if (songUri != null) {
            mediaPlayer.reset()
            mediaPlayer.setDataSource(applicationContext!!, Uri.parse(songUri))
            mediaPlayer.prepare()
            mediaPlayer.isLooping = true
            val intentAct = Intent("service-message")
            intentAct.putExtra("value", 2)
            intentAct.putExtra("songNumb", numbPlayingSong)
            LocalBroadcastManager.getInstance(this@RiderService).sendBroadcast(intentAct)
            if (playing) {
                mediaPlayer.start()
            }
        }
    }

    fun onUpdateSpeedBorder() {
        speedUp[0] = pref?.getString("speed_up1", "4")?.toDouble()!!
        speedUp[1] = pref?.getString("speed_up2", "7")?.toDouble()!!
        speedUp[2] = pref?.getString("speed_up3", "10")?.toDouble()!!

        speedDown[0] = pref?.getString("speed_down1", "2")?.toDouble()!!
        speedDown[1] = pref?.getString("speed_down2", "5")?.toDouble()!!
        speedDown[2] = pref?.getString("speed_down3", "7")?.toDouble()!!
    }

    fun changePlayState() {
        if (!playing) {
            mediaPlayer.start()
            notification(false)
            hasPlayed = true
            if (isDestroyed) {
                timer.cancel()
            }
        } else {
            mediaPlayer.pause()
            notification(true)
            hasPlayed = false
            if (isDestroyed) {
                serviceDestroy()
            }
        }
        clearArrays()
        playing = !playing
        pref?.edit()?.putBoolean("isPlaying", playing)?.apply()
    }

    private fun speedArrayRaise(a: Int) {
        var amount = 0
        if (a != -1) {
            amount = checkAmountRaise[a]
        }
        amount++
        checkAmountRaise[0] = 0
        checkAmountRaise[1] = 0
        checkAmountRaise[2] = 0
        checkAmountRaise[a] = amount
    }

    private fun clearArrays() {
        checkAmountRaise[0] = 0
        checkAmountRaise[1] = 0
        checkAmountRaise[2] = 0
        checkAmountFall[0] = 0
        checkAmountFall[1] = 0
        checkAmountFall[2] = 0
    }

    private fun speedArrayDown(a: Int) {
        var amount = 0
        if (a != -1) {
            amount = checkAmountFall[a]
        }
        amount++
        checkAmountFall[0] = 0
        checkAmountFall[1] = 0
        checkAmountFall[2] = 0
        checkAmountFall[a] = amount
    }

    private fun updateSpeed(location: CLocation?) {
        if (location != null) {
            location.setUseMetricunits(true)
            currentSpeed = location.speed.toDouble()
            when(numbPlayingSong) {
                1 -> {
                    checkSpeedRaiseSong()
                }
                2, 3 -> {
                    checkSpeedRaiseSong()
                    checkSpeedFall()
                }
                4 -> {
                    checkSpeedFall()
                }

            }
        }
    }

    private fun checkSpeedRaiseSong() {
        if (speedUp[numbPlayingSong - 1]!! < currentSpeed) {

            if(checkAmountRaise[numbPlayingSong - 1] == CHANGE_SPEED) {
                setNewTrack(numbPlayingSong + 1)
                clearArrays()
                return
            }
            speedArrayRaise(numbPlayingSong - 1)
        }
    }

    private fun checkSpeedFall() {
        if (speedDown[numbPlayingSong - 2]!! > currentSpeed) {

            if(checkAmountFall[numbPlayingSong - 2] == CHANGE_SPEED) {
                setNewTrack(numbPlayingSong - 1)
                clearArrays()
                return
            }
            speedArrayDown(numbPlayingSong - 2)
        }
    }

    fun serviceDestroy() {
        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (!hasPlayed && activityDestroyed) {
                    isDestroyed = true
                    stopForeground(true)
                    stopSelf()
                }
            }
        }, 10 * 60 * 1000L /*10 minutes*/)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val service = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(
            NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            )
        )
        return channelId
    }

    fun notification(pause: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val icon: Int
            val title : String
             if (pause) {
                 icon = R.drawable.ic_play_arrow
                 title = "Pause"
             }
            else {
                icon = R.drawable.ic_pause
                 title = "Playing"
             }

            val mediaSessionCompat = MediaSessionCompat(this, "tag")

            val intentPlay = Intent("Accept")
            val pendingIntentPlay = PendingIntent.getBroadcast(
                this@RiderService,
                0,
                intentPlay,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

            val notification = NotificationCompat.Builder(
                this, createNotificationChannel(
                    "my_service",
                    "My Background Service"
                )
            )
                    .setSmallIcon(R.drawable.ic_audiotrack_dark)
                    .setContentTitle("Rider")
                    .setContentText(title)
                    .addAction(icon, "Play", pendingIntentPlay)
                    .setStyle(
                        androidx.media.app.NotificationCompat.MediaStyle()
                            .setMediaSession(mediaSessionCompat.sessionToken)
                    )
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .build()
            startForeground(2001, notification)
            }

        if (!receiverExist) {
            receiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent) {
                    if (intent.action == "Accept") {
                        val intentAct = Intent("service-message")
                        if (playing) intentAct.putExtra("value", 0)
                        else intentAct.putExtra("value", 1)
                        changePlayState()
                        LocalBroadcastManager.getInstance(this@RiderService).sendBroadcast(intentAct)
                        notification(!playing)
                    }
                }
            }
            val intentFilter = IntentFilter()
            intentFilter.addAction("Accept")
            applicationContext.registerReceiver(receiver, intentFilter)
            receiverExist = true
        }
    }

    inner class LocalBinder : Binder() {
        fun getService(): RiderService = this@RiderService
    }

    override fun onLocationChanged(location: Location) {
        val myLocation = CLocation(location, true)
        updateSpeed(myLocation)
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        locationRequest = LocationRequest()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = 500
        locationRequest?.fastestInterval = 500
        LocationServices.FusedLocationApi.requestLocationUpdates(
            googleApiClient,
            locationRequest,
            this
        )
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }
}