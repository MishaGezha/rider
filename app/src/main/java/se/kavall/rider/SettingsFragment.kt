package se.kavall.rider

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat


class SettingsFragment : PreferenceFragmentCompat() {
    private var pref :SharedPreferences ?= null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        pref = this.activity?.getSharedPreferences("tracks", Context.MODE_PRIVATE)
        val uriDataAdapter = UriDataAdapter()
        for(i in 1..4) {
            val songUri = pref?.getString("song$i", null)
            val preference: Preference? = findPreference("song$i")
            if (songUri != null) {
                preference?.title = uriDataAdapter.getTitleAuth(Uri.parse(songUri), requireContext())
            }
            preference!!.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                setMusic(i)
                true
            }
        }

        for (i in 1..3) {
            val speedUp = pref?.getString("speed_up$i", null)
            val speedDown = pref?.getString("speed_down$i", null)
            val editTextUp = preferenceManager.findPreference<EditTextPreference>("song_up$i")
            val editTextDown = preferenceManager.findPreference<EditTextPreference>("song_down$i")
            if (speedUp != null) {
                editTextUp?.summary = "Speed barrier is set to $speedUp"
            }
            if (speedDown != null) {
                editTextDown?.summary = "Speed barrier is set to $speedDown"
            }
            editTextUp!!.setOnPreferenceChangeListener { preference, newValue ->
                preference.summary = "Speed barrier is set to $newValue"
                pref?.edit()?.putString("speed_up$i", newValue.toString())?.apply()
                activity?.let {
                    (it as MainActivity).mService.onUpdateSpeedBorder()
                }
                true
            }

            editTextDown!!.setOnPreferenceChangeListener { preference, newValue ->
                preference.summary = "Speed barrier is set to $newValue"
                pref?.edit()?.putString("speed_down$i", newValue.toString())?.apply()
                activity?.let {
                    (it as MainActivity).mService.onUpdateSpeedBorder()
                }
                true
            }
        }
    }



    private fun setMusic(key: Int) {
        val intent = Intent()
            .setType("audio/*")
            .setAction(Intent.ACTION_OPEN_DOCUMENT)
            .addCategory(Intent.CATEGORY_OPENABLE)

        startActivityForResult(Intent.createChooser(intent, "Select a file"), key)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val uri: Uri = data?.data!!
            val uriDataAdapter = UriDataAdapter()

            val takeFlags = data.flags and (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            activity?.contentResolver?.takePersistableUriPermission(uri, takeFlags)

            pref?.edit()?.putString("song$requestCode", uri.toString())?.apply()
            val preference: Preference? = findPreference("song$requestCode")

            val title = uriDataAdapter.getTitleAuth(uri, requireContext())
            preference?.title = title

            activity?.let {
                (it as MainActivity).mService.setNewTrack(1)
            }
        }
    }
}