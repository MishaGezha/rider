package se.kavall.rider

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
class UriDataAdapter {
    fun getFileTitle(uri: Uri, context: Context): String {
        var titleName: String? = null
        if (uri.scheme == "content") {
            val proj = arrayOf(MediaStore.Audio.Media.TITLE)
            val cursor: Cursor? = context.contentResolver?.query(uri, proj, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    titleName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                }
            } finally {
                cursor?.close()
            }
        }
        if (titleName == null) {

            titleName = uri.path
            val cut = titleName!!.lastIndexOf('/')
            if (cut != -1) {
                titleName = titleName.substring(cut + 1)
            }
        }
        return titleName
    }

    fun getTitleAuth(uri: Uri, context: Context): String {
        var titleName: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = context.contentResolver?.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    titleName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor?.close()
            }
        }
        if (titleName == null) {

            titleName = uri.path
            val cut = titleName!!.lastIndexOf('/')
            if (cut != -1) {
                titleName = titleName.substring(cut + 1)
            }
        }
        return titleName
    }

    fun getTrackAuthor(uri: Uri, context: Context): String? {
        var titleAuthor : String ?= null
        if (uri.scheme == "content") {
            val proj = arrayOf(MediaStore.Audio.Artists.ARTIST)
            val cursor: Cursor? = context.contentResolver?.query(uri, proj, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    titleAuthor = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                }
            } finally {
                cursor?.close()
            }
        }
        return titleAuthor
    }

    fun getAlbumImage(uri: Uri, context: Context): Uri? {
        var albumArtUri: Uri? = null
        if (uri.scheme == "content") {
            val proj = arrayOf(MediaStore.Audio.Media.ALBUM_ID)
            val cursor: Cursor? = context.contentResolver?.query(uri, proj, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    val albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID))
                    val sArtworkUri = Uri.parse("content://media/external/audio/albumart")
                    albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId)
                }
            } finally {
                cursor?.close()
            }
        }
        return albumArtUri
    }
}